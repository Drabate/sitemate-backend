#!/usr/bin/env node

const axios = require('axios');
const { program } = require('commander');

const apiUrl = 'http://localhost:3000';
const api = axios.create({
  baseURL: apiUrl,
});

program
  .command('create')
  .description('Create a new object')
  .action(async (data) => {
    try {
      const response = await api.post('/create',
      {
        id: 3,
        title: 'title 3',
        description: 'description 3'
      }
      );
      console.log('Created:', response.data);
    } catch (error) {
      console.error('Error creating:', error.message);
    }
  });

program
  .command('read')
  .description('Read an object')
  .action(async () => {
    try {
      const response = await api.get('/read');
      console.log('Read:', response.data);
    } catch (error) {
      console.error('Error reading:', error.message);
    }
  });

program
  .command('update')
  .description('Update an object')
  .action(async (data) => {
    try {
      const response = await api.put('/update', 
      {
        id: 3,
        title: 'title 3',
        description: 'description 4'
      }
      );
      console.log('Updated:', response.data);
    } catch (error) {
      console.error('Error updating:', error.message);
    }
  });

program
  .command('delete')
  .description('Delete an object')
  .action(async (data) => {
    try {
      const response = await api.delete('/delete',
      { data:
        { 
            id: 3 
        }
    }
      
    );
      console.log('Deleted:', response.data);
    } catch (error) {
      console.error('Error deleting:', error.message);
    }
  });

program.parse(process.argv);
