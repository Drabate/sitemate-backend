// app.js

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

const sampleObject = {
  id: 1,
  title: 'title',
  description: 'description'
};

app.post('/create', (req, res) => {
  const newObj = req.body;
  console.log('Created:', newObj);
  res.json(newObj);
});

app.get('/read', (req, res) => {
  res.json(sampleObject);
});

app.put('/update', (req, res) => {
  const updatedObj = req.body;
  console.log('Updated:', updatedObj);
  res.json(updatedObj);
});

app.delete('/delete', (req, res) => {
  const objToDelete = req.body;
  console.log('Deleted:', objToDelete);
  res.json(objToDelete);
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
